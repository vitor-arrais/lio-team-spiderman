package com.example.testapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import cielo.orders.domain.Credentials
import cielo.sdk.order.OrderManager
import cielo.sdk.order.ServiceBindListener
import android.widget.Toast
import cielo.orders.domain.Order
import cielo.sdk.order.payment.PaymentError
import cielo.sdk.order.payment.PaymentListener
import cielo.sdk.order.payment.PaymentCode
import cielo.orders.domain.CheckoutRequest
import android.util.Log
import android.view.View
import android.widget.Button
import org.jetbrains.annotations.NotNull


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val btnOrder = findViewById<Button>(R.id.btn_order)
        btnOrder.setOnClickListener(View.OnClickListener
        {
            createOrderManager()
        })

        val btnCancel = findViewById<Button>(R.id.btn_cancel)
        btnOrder.setOnClickListener(View.OnClickListener
        {
            createOrderManager()
        })

    }

    private fun createOrderManager() {
        val credentials = Credentials("na1bu1sW08FQ", "bI8QDRCkhSKE")
        val orderManager = OrderManager(credentials, this@MainActivity)

        val serviceBindListener = object : ServiceBindListener {

            override fun onServiceBoundError(throwable: Throwable) {
                Toast.makeText(this@MainActivity, "Error", Toast.LENGTH_SHORT).show()
            }

            override fun onServiceBound() {
                //Você deve garantir que sua aplicação se conectou com a LIO a partir desse listener
                //A partir desse momento você pode utilizar as funções do OrderManager, caso contrário uma exceção será lançada.

                //Deve ser modificada uma flag para saber se o serviço está funcionando?

                Toast.makeText(this@MainActivity, "Funcionou", Toast.LENGTH_SHORT).show()
                val order = createOrder(orderManager)

                orderManager.placeOrder(order)

                orderManager.checkoutOrder(getPaymentRequest(order),getPaymentListener())

            }

            override fun onServiceUnbound() {
                // O serviço foi desvinculado
            }
        }
        orderManager.bind(this@MainActivity, serviceBindListener)

        orderManager.unbind()
    }

    private fun getPaymentRequest(order: Order) : CheckoutRequest {
        val request = CheckoutRequest.Builder()
            .orderId(order.id) /* Obrigatório */
            .build()
        return request

        /*
         .amount(123456789) /* Opcional */
            .ec("999999999999999") /* Opcional (precisa estar habilitado na LIO) */
            .installments(3) /* Opcional */
            .email("teste@email.com") /* Opcional */
            .paymentCode(PaymentCode.CREDITO_PARCELADO_LOJA) /* Opcional */
         */
    }

    private fun getPaymentListener() : PaymentListener {
        val paymentListener = object : PaymentListener {
            override fun onStart() {
                Log.d("SDKClient", "O pagamento começou.")
            }

            override fun onPayment(@NotNull order: Order) {
                Log.d("SDKClient", "Um pagamento foi realizado.")
            }

            override fun onCancel() {
                Log.d("SDKClient", "A operação foi cancelada.")
            }

            override fun onError(@NotNull paymentError: PaymentError) {
                Log.d("SDKClient", "Houve um erro no pagamento.")
            }
        }
        return paymentListener
    }

    private fun createOrder(orderManager: OrderManager) : Order {
        val localOrder = orderManager.createDraftOrder("0001")
        val sku = "2891820317391823"
        val name = "refrigerante"
        val unitPrice = 550
        val quantity = 3
        val unityOfMeasure = "UNIDADE"

        localOrder!!.addItem(sku, name, unitPrice.toLong(), quantity, unityOfMeasure)

        return localOrder
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
